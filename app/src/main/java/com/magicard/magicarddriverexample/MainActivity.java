package com.magicard.magicarddriverexample;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.JobIntentService;
import androidx.core.content.ContextCompat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends Activity {

    //this string is passed to the intent creator; when we send a print to the driver,
    //this value will be displayed to the user, so they know what app produced the print request
    private static final String APPLICATION_ID = "hello world!";

    //a progress dialog; displayed while we are crunching the image files
    private WorkingDialog mWorkingDialog;

    //variable for extended card size
    private String extendedCardSize = "";

    //variable for printer model selection
    private boolean enduroFamilySelected = true;

    //Enum values for controlling thread behaviour
    private enum PrintMode{
        PRINT_SINGLE,
        PRINT_DUPLEX,
        PRINT_ENCODED,
        PRINT_EXTENDED,
        PRINT_SILENT
    }

    private final int REQUEST_PERMISSION = 14;

    private View permissionView;

    //helper method for transporting a raw resource file onto the device's storage
    private String saveRawFileToDisk(int resource, String name) throws IOException{
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File file = new File(extStorageDirectory, name);
            if (file.exists()) {
                file.delete();
            }

            InputStream in = getResources().openRawResource(resource);
            FileOutputStream out = new FileOutputStream(file.getAbsolutePath());
            byte[] buff = new byte[1024];
            int read;

            try {
                while ((read = in.read(buff)) > 0) {
                    out.write(buff, 0, read);
                }
            } finally {
                in.close();
                out.close();
            }
            return file.getAbsolutePath();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission granted - printing can now execute!", Toast.LENGTH_SHORT).show();
                    if (permissionView != null) {
                        permissionView.callOnClick();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mWorkingDialog = new WorkingDialog();

        findViewById(R.id.btn_open_settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permissionView = v;
                Intent i = Intents.getDriverIntent(Intents.DRIVER_INTENT_OPEN_DRIVER_SETTINGS, APPLICATION_ID, getPackageManager());
                if (i != null) {
                    if (enduroFamilySelected) {
                        i.putExtra(Intents.DRIVER_EXTRA_MODEL_ENDURO, true);
                    } else {
                        i.putExtra(Intents.DRIVER_EXTRA_MODEL_PRO_360, true);
                    }
                    System.out.println("Opening the driver app, and displaying the 'print settings'");
                    startActivity(i);
                } else {
                    System.out.println("Driver is not installed! Goto: https://play.google.com/store/apps/details?id=com.magicard.driver");
                }
            }
        });

        findViewById(R.id.btn_open_comms).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permissionView = v;
                Intent i = Intents.getDriverIntent(Intents.DRIVER_INTENT_OPEN_PRINTER_SETTINGS, APPLICATION_ID, getPackageManager());
                if (i != null) {
                    if (enduroFamilySelected) {
                        i.putExtra(Intents.DRIVER_EXTRA_MODEL_ENDURO, true);
                    } else {
                        i.putExtra(Intents.DRIVER_EXTRA_MODEL_PRO_360, true);
                    }
                    System.out.println("Opening the driver app, and displaying the 'comms settings'");
                    startActivity(i);
                } else {
                    System.out.println("Driver is not installed! Goto: https://play.google.com/store/apps/details?id=com.magicard.driver");
                }
            }
        });

        findViewById(R.id.btn_open_queue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permissionView = v;
                Intent i = Intents.getDriverIntent(Intents.DRIVER_INTENT_OPEN_PRINTER_SPOOLER, APPLICATION_ID, getPackageManager());
                if (i != null) {
                    if (enduroFamilySelected) {
                        i.putExtra(Intents.DRIVER_EXTRA_MODEL_ENDURO, true);
                    } else {
                        i.putExtra(Intents.DRIVER_EXTRA_MODEL_PRO_360, true);
                    }
                    System.out.println("Opening the driver app, and displaying the 'job queue'");
                    startActivity(i);
                } else {
                    System.out.println("Driver is not installed! Goto: https://play.google.com/store/apps/details?id=com.magicard.driver");
                }
            }
        });

        findViewById(R.id.btn_print_single).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permissionView = v;
                new PrintCard().execute(PrintMode.PRINT_SINGLE);
            }
        });

        findViewById(R.id.btn_print_duplex).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permissionView = v;
                new PrintCard().execute(PrintMode.PRINT_DUPLEX);
            }
        });

        findViewById(R.id.btn_print_encode).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permissionView = v;
                new PrintCard().execute(PrintMode.PRINT_ENCODED);
            }
        });

        findViewById(R.id.btn_print_extended).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permissionView = v;
                AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                final View layout = View.inflate(MainActivity.this, R.layout.dialog_extended, null);
                dialog.setView(layout);
                final EditText etExtended = (EditText) layout.findViewById(R.id.extended_input);
                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int integer) {
                        extendedCardSize = etExtended.getText().toString();
                        new PrintCard().execute(PrintMode.PRINT_EXTENDED);
                    }
                });
                dialog.setNegativeButton("Cancel", null);
                dialog.show();
            }
        });

        findViewById(R.id.btn_print_silent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permissionView = v;
                new PrintCard().execute(PrintMode.PRINT_SILENT);
            }
        });

        findViewById(R.id.rb_printer_enduro).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enduroFamilySelected = true;
            }
        });

        findViewById(R.id.rb_printer_pro360).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enduroFamilySelected = false;
            }
        });
    }

    private class PrintCard extends AsyncTask<PrintMode, Void, Intent>{

        private String mCardFrontColour;
        private String mCardFrontResin;
        private String mCardBackColour;
        private String mCardBackResin;
        private PrintMode mPrintMode;

        @Override
        protected void onPreExecute() {
            if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Storage required to read PRN file in the driver app");
                    builder.setNegativeButton("Cancel", null);
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    REQUEST_PERMISSION);
                        }
                    });
                    builder.show();
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION);
                }
                this.cancel(true);
            } else {
                mWorkingDialog.show(getFragmentManager(), this.getClass().toString());
            }
        }

        @Override
        protected Intent doInBackground(PrintMode... params) {
            mPrintMode = params[0];
            //printing silently (via the service) requires the BATCH_PRINT flag; everything else uses the regular INTENT_PRINT flag
            Intent i = Intents.getDriverIntent((mPrintMode == PrintMode.PRINT_SILENT) ? Intents.DRIVER_INTENT_BATCH_PRINT : Intents.DRIVER_INTENT_PRINT, APPLICATION_ID, getPackageManager());
            System.out.println("DRIVER_INTENT_PRINT: " + Intents.DRIVER_INTENT_PRINT);
            if(mPrintMode == null || i == null){
                //driver is not installed / cannot be found, or no printmode supplied; abort!
                return null;
            }

            try {
                //heavy work - transport the demo image data from the raw folder, onto the device's storage
                mCardFrontColour = saveRawFileToDisk(R.raw.testcard_front, "testcard_front.png");
                mCardFrontResin = saveRawFileToDisk(R.raw.testcard_front_resin, "testcard_front_resin.png");
                if(mPrintMode == PrintMode.PRINT_DUPLEX){
                    mCardBackColour = saveRawFileToDisk(R.raw.testcard_back, "testcard_back.png");
                    mCardBackResin = saveRawFileToDisk(R.raw.testcard_back_resin, "testcard_back_resin.png");
                    MediaScannerConnection.scanFile(getApplicationContext(), new String[]{
                            mCardFrontColour,
                            mCardFrontResin,
                            mCardBackColour,
                            mCardBackResin}, null, null);
                }else{
                    MediaScannerConnection.scanFile(getApplicationContext(), new String[]{
                            mCardFrontColour,
                            mCardFrontResin}, null, null);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return i;
        }

        @Override
        protected void onPostExecute(final Intent i) {
            //determine if driver is installed
            if(i == null){
                Toast.makeText(getApplicationContext(), "Driver is not installed! Goto: https://play.google.com/store/apps/details?id=com.magicard.v2driver", Toast.LENGTH_LONG).show();
                return;
            }

            if (enduroFamilySelected) {
                i.putExtra(Intents.DRIVER_EXTRA_MODEL_ENDURO, true);
            } else {
                i.putExtra(Intents.DRIVER_EXTRA_MODEL_PRO_360, true);
            }
            //add card front image metadata
            //put filepath location of bitmap containing the COLOUR (YMC + composite) data of the FRONT of the card
            i.putExtra(Intents.DRIVER_EXTRA_FILEPATH, mCardFrontColour);
            //put filepath location of bitmap containing the RESIN (K) data of the FRONT of the card
            i.putExtra(Intents.DRIVER_EXTRA_FILEPATH_RESIN, mCardFrontResin);

            //add card back (duplex) image metadata
            if(mPrintMode == PrintMode.PRINT_DUPLEX){
                //put filepath location of bitmap containing the COLOUR (YMC + composite) data of the BACK of the card (DUPLEX only)
                i.putExtra(Intents.DRIVER_EXTRA_FILEPATH_BACK, mCardBackColour);
                //put filepath location of bitmap containing the RESIN (K) data of the BACK of the card (DUPLEX only)
                i.putExtra(Intents.DRIVER_EXTRA_FILEPATH_BACK_RESIN, mCardBackResin);
            }

            //add xtended print metadata
            if(mPrintMode == PrintMode.PRINT_EXTENDED) {
                if (!extendedCardSize.equals("")) {
                    i.putExtra(Intents.DRIVER_EXTRA_EXTENDED_SIZE, extendedCardSize);
                }
                i.putExtra(Intents.DRIVER_EXTRA_EXTENDED_PRINT, true);
            }

            //add encoding metadata
            if(mPrintMode == PrintMode.PRINT_ENCODED){
                //Instruct the driver to encode magnetic data before printing this card
                //(Optional; false by default)
                i.putExtra(Intents.DRIVER_EXTRA_ENCODING_ENABLED, true);
                i.putExtra(Intents.DRIVER_EXTRA_ENCODING_TRACK1, "Track1");
                i.putExtra(Intents.DRIVER_EXTRA_ENCODING_TRACK2, "1234");
                i.putExtra(Intents.DRIVER_EXTRA_ENCODING_TRACK3, "5678");
            }

            //compile success message
            String message = "a single sided card";
            String prefix = "Opening the driver app, and printing ";
            if(mPrintMode == PrintMode.PRINT_DUPLEX){
                message = "a duplex card";
            }else if(mPrintMode == PrintMode.PRINT_ENCODED){
                message = "an encoded card";
            }else if(mPrintMode == PrintMode.PRINT_EXTENDED){
                message = "an extended card";
            } else if(mPrintMode == PrintMode.PRINT_SILENT){
                prefix = "Printing ";
                message = "a card, silently, via the driver service (open the driver manually, to confirm)";
            }

            //indicates success to the user, dismiss the progress dialog
            Toast.makeText(getApplicationContext(), String.format("%s%s", prefix, message), Toast.LENGTH_LONG).show();
            mWorkingDialog.dismiss();

            //fire the intent
            if(mPrintMode == PrintMode.PRINT_SILENT){
                startService(i);
            }else {
                startActivity(i);
            }
        }
    }
}
