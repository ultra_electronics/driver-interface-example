package com.magicard.magicarddriverexample;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;

/**
 * Created by Gareth.Sutton on 26/01/2016.
 */
public class WorkingDialog extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View layout = getActivity().getLayoutInflater().inflate(R.layout.dialog_working, null);
        builder.setView(layout);
        setCancelable(false);
        return builder.create();
    }
}
