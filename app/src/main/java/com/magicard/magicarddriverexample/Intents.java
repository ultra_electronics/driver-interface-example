package com.magicard.magicarddriverexample;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;

public class Intents {

    private static String TAG = "Intents";
    //the driver app package name - as defined in the manifest
    public static final String DRIVER_PACKAGE_NAME = "com.magicard.v2driver";


    //Opens the spooler; uses intent data to generate a PRN and add it to the print queue
    //ideal for sending a single print to the driver
    public static final String DRIVER_INTENT_PRINT = "com.magicard.v2driver.PRINT_CARD";


    //silently adds a batch print job, and starts a service to generate a PRN in the background
    //ideal for sending multiple prints to the driver
    public static final String DRIVER_INTENT_BATCH_PRINT = "com.magicard.v2driver.BATCH_PRINT";

    //silently changes the settings, ideal for making changes from an external application
    public static final String DRIVER_INTENT_SETTINGS = "com.magicard.v2driver.SETTINGS";

    //Opens the driver settings page
    public static final String DRIVER_INTENT_OPEN_DRIVER_SETTINGS = "com.magicard.v2driver.OPEN_DRIVER_SETTINGS";
    //Opens the printer settings page
    public static final String DRIVER_INTENT_OPEN_PRINTER_SETTINGS = "com.magicard.v2driver.OPEN_PRINTER_SETTINGS";
    //Opens the printer spooler
    public static final String DRIVER_INTENT_OPEN_PRINTER_SPOOLER = "com.magicard.v2driver.OPEN_PRINTER_SPOOLER";

    public static final String DRIVER_BATCH_RECIEVER_CLASS_NAME = "com.magicard.v2driver.printservice.BatchReceiverService";
    public static final String DRIVER_SETTINGS_RECEIVER_CLASS_NAME = "com.magicard.v2driver.printservice.SettingsReceiverService";

    public static final String DRIVER_EXTRA_EXTENDED_PRINT = "com.magicard.PRINT_XTD_CARD";
    public static final String DRIVER_EXTRA_EXTENDED_SIZE = "com.magicard.PRINT_XTD_SIZE";

    //constants for bundle extras
    public static final String DRIVER_EXTRA_MODEL_PRO_360 = "com.magicard.v2driver.PRINT_CARD.pro360";
    public static final String DRIVER_EXTRA_MODEL_ENDURO = "com.magicard.v2driver.PRINT_CARD.enduro";
    public static final String DRIVER_EXTRA_FILEPATH = "com.magicard.v2driver.PRINT_CARD.filepath";
    public static final String DRIVER_EXTRA_FILEPATH_RESIN = "com.magicard.v2driver.PRINT_CARD.filepath.resin";
    public static final String DRIVER_EXTRA_FILEPATH_BACK = "com.magicard.v2driver.PRINT_CARD.filepath.back";
    public static final String DRIVER_EXTRA_FILEPATH_BACK_RESIN = "com.magicard.v2driver.PRINT_CARD.filepath.back.resin";
    public static final String DRIVER_EXTRA_APPLICATION_ID = "com.magicard.v2driver.PRINT_CARD.APPLICATION_ID";
    public static final String DRIVER_EXTRA_ENCODING_ENABLED = "com.magicard.v2driver.PRINT_CARD.encodingenabled";
    public static final String DRIVER_EXTRA_ENCODING_TRACK1 = "com.magicard.v2driver.PRINT_CARD.track1";
    public static final String DRIVER_EXTRA_ENCODING_TRACK2 = "com.magicard.v2driver.PRINT_CARD.track2";
    public static final String DRIVER_EXTRA_ENCODING_TRACK3 = "com.magicard.v2driver.PRINT_CARD.track3";

    //constants for encoding optional extras
    public static final String DRIVER_EXTRA_ENCODING_COERC = "com.magicard.v2driver.PRINT_CARD_coerc";
    public static final String DRIVER_EXTRA_ENCODING_VERIFY = "com.magicard.v2driver.PRINT_CARD_verify";
    public static final String DRIVER_EXTRA_ENCODING_TRACK1_MPC = "com.magicard.v2driver.PRINT_CARD.track1.mpc";
    public static final String DRIVER_EXTRA_ENCODING_TRACK1_BPI = "com.magicard.v2driver.PRINT_CARD.track1.bpi";
    public static final String DRIVER_EXTRA_ENCODING_TRACK2_MPC = "com.magicard.v2driver.PRINT_CARD.track2.mpc";
    public static final String DRIVER_EXTRA_ENCODING_TRACK2_BPI = "com.magicard.v2driver.PRINT_CARD.track2.bpi";
    public static final String DRIVER_EXTRA_ENCODING_TRACK3_MPC = "com.magicard.v2driver.PRINT_CARD.track3.mpc";
    public static final String DRIVER_EXTRA_ENCODING_TRACK3_BPI = "com.magicard.v2driver.PRINT_CARD.track3.bpi";

    // constants for sending settings to the driver
    public static final String DRIVER_EXTRA_SETTING_CPW = "com.magicard.v2driver.SETTING.cpw";
    public static final String DRIVER_EXTRA_SETTING_RPW = "com.magicard.v2driver.SETTING.rpw";
    public static final String DRIVER_EXTRA_SETTING_OPW = "com.magicard.v2driver.SETTING.opw";
    public static final String DRIVER_EXTRA_SETTING_NOC = "com.magicard.v2driver.SETTING.noc";
    public static final String DRIVER_EXTRA_SETTING_IMAGE_YAXIS = "com.magicard.v2driver.SETTING.yax";
    public static final String DRIVER_EXTRA_SETTING_IMAGE_START = "com.magicard.v2driver.SETTING.imgstrt";
    public static final String DRIVER_EXTRA_SETTING_IMAGE_END = "com.magicard.v2driver.SETTING.imgend";
    public static final String DRIVER_EXTRA_SETTING_RESIN_THRESHOLD = "com.magicard.v2driver.SETTING.resin.threshold";
    public static final String DRIVER_EXTRA_SETTING_INCLUDE_COMPOSITE = "com.magicard.v2driver.SETTING.includecomposite";
    public static final String DRIVER_EXTRA_SETTING_FRONT_COLOUR_FORMAT = "com.magicard.v2driver.SETTING.colourformat.front";
    public static final String DRIVER_EXTRA_SETTING_BACK_COLOUR_FORMAT = "com.magicard.v2driver.SETTING.colourformat.back";
    public static final String DRIVER_EXTRA_SETTING_OVERCOAT_FRONT_ENABLED = "com.magicard.v2driver.SETTING.frontovercoat";
    public static final String DRIVER_EXTRA_SETTING_OVERCOAT_BACK_ENABLED = "com.magicard.v2driver.SETTING.backovercoat";
    public static final String DRIVER_EXTRA_SETTING_HOLOKOTE_ID_FRONT = "com.magicard.v2driver.SETTING.holokote.frontid";
    public static final String DRIVER_EXTRA_SETTING_HOLOKOTE_ID_BACK = "com.magicard.v2driver.SETTING.holokote.backid";
    public static final String DRIVER_EXTRA_SETTING_HOLOKOTE_MAP_FRONT = "com.magicard.v2driver.SETTING.holokote.frontmap";
    public static final String DRIVER_EXTRA_SETTING_HOLOKOTE_MAP_BACK = "com.magicard.v2driver.SETTING.holokote.backmap";
    public static final String DRIVER_EXTRA_SETTING_HOLOKOTE_ROTATION_FRONT = "com.magicard.v2driver.SETTING.holokote.front.rotation";
    public static final String DRIVER_EXTRA_SETTING_HOLOKOTE_ROTATION_BACK = "com.magicard.v2driver.SETTING.holokote.back.rotation";
    public static final String DRIVER_EXTRA_SETTING_HOLOPATCH_POSITION = "com.magicard.v2driver.SETTING.holopatch.position";

    // constants for receiving settings from the driver
    public static final String DRIVER_EXTRA_SETTING_GET_POWER_LEVELS = "com.magicard.v2driver.SETTING.powerlevels.get";
    public static final String DRIVER_EXTRA_SETTING_GET_IMAGE_POSITIONING = "com.magicard.v2driver.SETTING.imagepos.get";
    public static final String DRIVER_EXTRA_SETTING_GET_RESIN_VALUES = "com.magicard.v2driver.SETTING.resin.get";
    public static final String DRIVER_EXTRA_SETTING_GET_PRINT_SETTINGS = "com.magicard.v2driver.SETTING.printsettings.get";
    public static final String DRIVER_EXTRA_SETTING_GET_CARD_SETTINGS_FRONT = "com.magicard.v2driver.SETTING.cardsettings.front.get";
    public static final String DRIVER_EXTRA_SETTING_GET_CARD_SETTINGS_BACK = "com.magicard.v2driver.SETTING.cardsettings.back.get";
    public static final String DRIVER_EXTRA_SETTING_GET_HOLOKOTE_FRONT = "com.magicard.v2driver.SETTING.holokote.front.get";
    public static final String DRIVER_EXTRA_SETTING_GET_HOLOKOTE_BACK = "com.magicard.v2driver.SETTING.holokote.back.get";
    public static final String DRIVER_EXTRA_SETTING_GET_HOLOPATCH = "com.magicard.v2driver.SETTING.holopatch.get";

    // Broadcast receiver constants for receiving data
    public static final String DRIVER_BROADCAST_RECEIVER_POWER_LEVELS = "MagicardPrinter-PowerLevel";
    public static final String DRIVER_BROADCAST_RECEIVER_IMAGE_POSITIONING = "MagicardPrinter-ImagePositioning";
    public static final String DRIVER_BROADCAST_RECEIVER_RESIN_VALUES = "MagicardPrinter-ResinValues";
    public static final String DRIVER_BROADCAST_RECEIVER_PRINT_SETTINGS = "MagicardPrinter-PrintSettings";
    public static final String DRIVER_BROADCAST_RECEIVER_CARD_SETTINGS_FRONT = "MagicardPrinter-CardSettingsFront";
    public static final String DRIVER_BROADCAST_RECEIVER_CARD_SETTINGS_BACK = "MagicardPrinter-CardSettingsBack";
    public static final String DRIVER_BROADCAST_RECEIVER_HOLOKOTE_FRONT = "MagicardPrinter-HolokoteFront";
    public static final String DRIVER_BROADCAST_RECEIVER_HOLOKOTE_BACK = "MagicardPrinter-HolokoteBack";
    public static final String DRIVER_BROADCAST_RECEIVER_HOLOPATCH = "MagicardPrinter-Holopatch";

    public static Intent getDriverIntent(String action, String applicationID, PackageManager pm) {
        //launch the driver app
        Intent i = pm.getLaunchIntentForPackage(Intents.DRIVER_PACKAGE_NAME);
        if (i == null) {
            Log.e(TAG, "Unable to resolve intent, is the driver installed?");
        } else {
            switch (action) {
                case DRIVER_INTENT_BATCH_PRINT:
                    //specifically start the batch printing service (silent)
                    i = new Intent();
                    i.setClassName(DRIVER_PACKAGE_NAME, DRIVER_BATCH_RECIEVER_CLASS_NAME);
                    i.putExtra(Intents.DRIVER_EXTRA_APPLICATION_ID, applicationID);
                    break;
                case DRIVER_INTENT_SETTINGS:
                    //start the service to send/receive settings (silent)
                    i = new Intent();
                    i.setClassName(DRIVER_PACKAGE_NAME, DRIVER_SETTINGS_RECEIVER_CLASS_NAME);
                    i.putExtra(Intents.DRIVER_EXTRA_APPLICATION_ID, applicationID);
                    break;
                default:
                    //start and display the driver application, and let the driver handle the routing via 'action' flag
                    i.setAction(action);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra(Intents.DRIVER_EXTRA_APPLICATION_ID, applicationID);
                    break;
            }
        }
        return i;
    }
}
